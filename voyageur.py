import TSP_biblio, math

def matrice_distances(fichier):
    """
    :author: Stéphane Devernay
    :date: Mai 2019
    :param fichier: (file name) nom du fichier txt contenat les données
    :return: (list) liste double des distances entre villes deux à deux
    :CU: utilise TSP_biblio
    """
    liste_villes = TSP_biblio.get_tour_fichier(fichier)
    l = len(liste_villes)
    distances = [[0]*l for i in range(l)]
    for i in range(l):
        for j in range(l ):
            if(i!=j):
                distances[i][j] =TSP_biblio.distance(liste_villes,i,j)
            else:
                distances[i][j] = 99999 # pour que la ville elle-même ne soit pas la plus proche
    return distances

def ville_plus_proche(ville,liste_ville,matrice):
    """
    :param ville: (int) numero de la ville
    :param villes: (liste) cette liste des numéros de villes décroit à chaque appel récursif
    :param matrice ( liste): matrice des distances entre toutes les villes
    :return: (list) la ville la plus proche du param ville
    >>> ville_plus_proche(10,[k for k in range(24)],matrice_distances('exemple.txt'))
    9
    """
    distance_min = min(matrice[liste_ville.index(ville)])
    ville = liste_ville[matrice[liste_ville.index(ville)].index(distance_min)]
    return ville

def tour(depart,villes,matrice):
    """
        renvoie une liste des villes [13,3,6....] à parcourir dans l'ordre depuis la ville départ. Ordre construit
        en retenant à chaque fois la ville suivante la plus proche en excluant celles déjà visitées.
        param (int) depart: ville (Annecy  0; Auxerre 1; Bastia  2; Bordeaux    3 ;Boulogne    4;Brest   5;
        Caen    6; Grenoble    7; Le Havre    8 ; Lens    9; Lille   10; Lyon    11; Paris   12 ;Lyon    13;
        Marseille   14; Metz    15; Nantes  16 ; Nancy   17; Nice    18; Rennes  19; Strasbourg  20;Saint-Etienne  21;
        Sedan   22;Toulouse    23
        :param (liste) villes: liste des numéros de villes initialement de 0 à 23
        :param (double liste): matrice des distances entre toutes les villes
        >>> tour(10,[k for k in range( 24)], matrice_distances('exemple.txt'))
        [10, 9, 4, 8, 6, 19, 16, 3, 23, 21, 11, 13, 7, 0, 18, 14, 2, 20, 17, 15, 22, 1, 12, 5, 5]
    """
    itineraire = []
    itineraire.append(depart)
    if len(villes) == 0:
        return itineraire
    else:
        etape =  ville_plus_proche(depart,villes,matrice)
        for i in range(len(matrice)):
            matrice[i].remove(matrice[i][villes.index(depart)]) # on enlève ce qui concerne la ville départ de la matrice
        matrice.remove(matrice[villes.index(depart)])
        villes.remove(depart)                                 # on enlève la ville départ de la liste
        itineraire = itineraire + tour(etape,villes,matrice) # appel récursif
        return itineraire

def trajet(depart):
    """
    renvoie la distance totale de la boucle
    affiche une fenetre pylab présentant le trajet à travers les villles
    :param depart: (int) entier entre 0 et 23 correspondant à la ville
    
    """
    itineraire_lat_long=[]
    itineraire = tour(depart,[k for k in range(24)],matrice_distances('exemple.txt'))
    for i in range(len(itineraire)):
        itineraire_lat_long.append(TSP_biblio.get_tour_fichier('exemple.txt')[itineraire[i]])
    print(TSP_biblio.longueur_tour(itineraire_lat_long))
    TSP_biblio.trace(itineraire_lat_long)
    return TSP_biblio.longueur_tour(itineraire_lat_long)

def tous_trajets():
    """
    :return: (list) liste contenant les distances parcourues pour tous les départs
    : affiche une fenetre matplotlib pour chaque parcours
    """
    trajets = []
    for i in range(24):
        trajets.append((trajet(i),i))
    return trajets

def min_trajet():
    """
    :return: le plus petit parcours (désactiver trace dans trajet()) et son numéro
    """   
    return min(tous_trajets())

def max_trajet():
    """
    :return: le plus grand parcours (désactiver trace dans trajet()) et son numéro
    """
    return max(tous_trajets())

if __name__ == '__main__':
    import doctest
    doctest.testmod()
        
    